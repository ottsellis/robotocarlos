
#include <fcntl.h>
#include <termios.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>

//Networking
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>

#include <cv.h>
#include <highgui.h>


#include "cmvision/capture.h"
#include "cmvision/vision.h"

//Serial Definitions
#define BAUDRATE B115200
#define SERIALDEVICE1 "/dev/SERIAL0"
#define SERIALDEVICE2 "/dev/SERIAL1"
#define SERIALDEVICE3 "/dev/SERIAL2"
#define SERIALDEVICE4 "/dev/SERIAL3"

#define SERIAL_TO_QUERY_HAS_BALL 2
#define SERIAL_TO_QUERY_WHICH_GOAL 1
#define SERIAL_TO_QUERY_ROBOT_GOING 1
#define SERIAL_TO_COILGUN 4
#define SERIAL_TO_ARN 1
#define _POSIX_SOURCE 1
#define PI 3.14159265


//Movement definitions
#define TURN_SPEED_WITHOUT_BALL 40
#define TURN_SPEED_WITH_BALL 27
#define TURN_SPEED_WITH_BALL2 15

//Vision definitions
#define VIDEODEVICE "/dev/camera"
#define WIDTH 640
#define HEIGHT 480
#define TMAPFILE "config/pseye2.tm"
#define COLORFILE "config/pseye.col"
	
#define filename1 "log1"
#define filename2 "log2"
#define filename3 "log3"
#define filename4 "log4"
		
#define green 1
#define ball 2
#define yellow 3
#define blue 4
#define white 5
#define black 6

#define WHITEBALANCE 3350
#define EXPOSURE 180
#define SATURATION 215
#define GAIN 0

#define PORT 80

#define CARLOS 1
#define GUI 2
#define NETWORKING 3

//Forward declarations
        //General
                int init(int type);
                int output(char *buffer);
                int close();
		
				int char_to_int(char c);
        //Serial
                int init_serial();
                char get_id_from_serial(int serial);
                int set_has_ball_from_serial();
                int set_robot_going_from_serial();
				int init_read_serial();
                void *read_serial_arn(void *arg);
                void *read_serial_1(void *arg);
                void *read_serial_2(void *arg);
                void *read_serial_3(void *arg);
                int set_read_serial(int i);
                int set_additional_serial_functions();
                int switch_serials();
				void close_read_serial();
                int close_serial();

        //Movement
                int init_movement();
                int stop();
                int forward(double velocity,double angle);
                int turn_with_ball(int i);
                int turn_without_ball(int i);
				int turn_with_ball2(int i);
				int turn_with_ball3(int i,double speed,double cof1,double cof2);

				double calculate_angle_modifier(int y);
				double calculate_angle(int x);
				double calculate_speed(int y);

				int start_robot();
				void *robot(void *arg);
				void close_robot();
				int get_ball(int &x,int &y);
				int check_ball(int x,int y);
				int check_ball_in_way(int x,int y);
				int check_edge();
				int check_goal_black_area();
        //GUI
                int init_gui();
                void show_image(char *buf,int isrgb);
                void show_blank();
                void draw_object(int x1,int y1,int x2,int y2);
                void show_gui();
				void close_gui();
				int gui();
        //Vision

                int init_vision();
                void close_vision();
                void process_frame();

                int check_ball(int x,int y);

                void *find_ball(void *arg);
                void start_find_ball();

        //Networking
                int init_socket();
                int send_data(char *buf);
                int recv_data();
                void * recieving_data(void *arg);
                int start_socket_threads();
				int send_image();
				int networking();
				void close_networking();
        //File
                int init_logging();
				int log(FILE* file,int id,char buffer[255]);
                int log1(int id,char buffer[255]);
                int log2(int id,char buffer[255]);
                int log3(int id,char buffer[255]);
                int log4(int id,char buffer[255]);
                int close_logging();

		//CARLOS
				int Carlos();
				
//Variable declarations
        // General variables
	
                int main_going;
                int robot_going;
				int is_robot_set;
				int just_started;

                int has_ball;
                int has_goal;
                int which_goal; //1=YELLOW; 2=BLUE;
                pthread_t tid[7];

        //Serial variables
                struct termios oldtio1,oldtio2,oldtio3,oldtio4,newtio;
		
                int serial1;
                int serial2;
                int serial3;
                int serial4;
                int serial_has_ball;
                int serial_which_goal;
                int serial_robot_going;
                int serial_arn;
				int serial_1;
				int serial_2;
				int serial_3;
                int serial_coilgun;

                int is_read_serial_arn_set;
				int is_read_serial_1_set;
				int is_read_serial_2_set;
				int is_read_serial_3_set;
		
				int is_serial_read_set;
                int is_serial_set;


        //Movement variables

                char command_turn_without_ball_right[7];
                char command_turn_without_ball_left[6];
                char command_turn_with_ball_left[6];
                char command_turn_with_ball_right[7];
                char command_turn_with_ball_left2[6];
                char command_turn_with_ball_right2[7];
		
                int command_turn_without_ball_right_length;
                int command_turn_without_ball_left_length;
                int command_turn_with_ball_right_length;
                int command_turn_with_ball_left_length;
                int command_turn_with_ball_right_length2;
                int command_turn_with_ball_left_length2;
		
                int is_movement_set;

				int side_to_search_goal;
		
        //GUI variables
                IplImage* rgbimg;
                IplImage* yuvimg;
                int imgcount;
				int is_gui_set;

        //CMVision variables

                Capture capture;
                LowVision vision;

                const CMVision::region *checkreg, *Ball, *blackline, *goal,* Whiteline,*check_ball_region,*goal_black_area;
                int is_vision_set;
                int whitbalance=WHITEBALANCE;
                int exposure=EXPOSURE;
                int gain=GAIN;
                int saturation=SATURATION;

                int find_ball_set;
                int find_goal_set;

        //Networking variables
				char SERVER[15];
                int is_socket_set;
                int sock;
                struct sockaddr_in server, from;
                unsigned int length;
                int recieving_data_going;



        //File
                FILE *file1;
                FILE *file2;
                FILE *file3;
                FILE *file4;

				
                int is_logging_set;
				int is_log1_set;
				int is_log2_set;
				int is_log3_set;
				int is_log4_set;
//Functions


// General

int init(int type){
	switch (type){
		case 1:
			robot_going=-1;
			is_gui_set=-1;
			main_going=1;
			if((is_logging_set=init_logging())!=1){
				main_going=0;
				return -1;
			}
			if((is_serial_set=init_serial())!=1){
				main_going=0;
				return -1;
			}
			if((is_serial_read_set=init_read_serial())!=1){
				main_going=0;
				return -1;
			}
			if((is_movement_set=init_movement())!=1){
				main_going=0;
				return -1;
			}
			if((is_vision_set=init_vision())!=1){
				main_going=0;
				return -1;
			}
			
			return 1;
		case 2:
			if((is_vision_set=init_vision())!=1){
				return -1;
			}
			if((is_gui_set=init_gui())!=1){
				return -1;
			}
			return 1;
			break;
		case 3:
			main_going=1;
			if((is_socket_set=init_socket())!=1){
				main_going=0;
				return -1;
			}
			if((is_logging_set=init_logging())!=1){
				main_going=0;
				return -1;
			}
			if((is_serial_set=init_serial())!=1){
				main_going=0;
				return -1;
			}
			if((is_serial_read_set=init_read_serial())!=1){
				main_going=0;
				return -1;
			}
			if((is_movement_set=init_movement())!=1){
				main_going=0;
				return -1;
			}
			if((is_vision_set=init_vision())!=1){
				main_going=0;
				return -1;
			}
			return 1;			
			break;
		default:
			return -1;
			break;
	}

}

int close(){
	output("About to exit");
	output("Closing logging");
	close_logging();
	output("Closing logging: done");
	output("Closing serial threads");
	close_read_serial();
	output("Closing serial threads: done");
	output("Closing serials");
	close_serial();
	output("Closing serials: done");
	output("Closing gui");
	close_gui();
	output("Closing gui: done");
	output("Closing robot");
	close_robot();
	output("Closing robot: done");
	output("Closing vision");
	close_vision();
	output("Closing vision: done");
	output("Closing networking");
	close_networking();
	output("Closing networking: done");
	output("Sleeping for a second just in case");
	sleep(1);
	output("Sleeping for a second just in case: done");
	return 1;
}

int output(char *buffer){
	if(is_socket_set==1){
		printf("%s\n",buffer);
		send_data(buffer);
	}
	else{
		printf("%s\n",buffer);
	}
}

int char_to_int(char c){
	return (int)((int) c-(int)'0');
}

//-------General------------------------

//-----------File---------------------------

int init_logging(){
	if(is_logging_set!=1){
		file1=fopen(filename1,"a+");
		file2=fopen(filename2,"a+");
		file3=fopen(filename3,"a+");
		file4=fopen(filename4,"a+");
		
		if(file1==NULL || file2==NULL || file3==NULL || file4==NULL){
			output("Error opening file");
			return -1;
		}
		else{
			is_log1_set=1;
			is_log2_set=1;
			is_log3_set=1;
			is_log4_set=1;
			return 1;
		}
	}
	else{
		return -1;
	}
}

int log(FILE* file,int id,char buffer[255]){
	char buf[80];
	time_t rawtime;
	struct tm * timeinfo;
	
	time(&rawtime);
	timeinfo=localtime(&rawtime);
	strftime(buf,80,"%H:%M:%S",timeinfo);
	
	fprintf(file,"at: %s from: %i message: %s\n",buf,id,buffer);
	fflush(file);
}

int log1(int id,char buffer[255]){
	if(is_log1_set==1){
		log(file1,id,buffer);
	}
}

int log2(int id,char buffer[255]){
	if(is_log2_set==1){
		log(file2,id,buffer);
	}
}

int log3(int id,char buffer[255]){
	if(is_log3_set==1){
		log(file3,id,buffer);
	}
}

int log4(int id,char buffer[255]){
	if(is_log4_set==1){
		log(file4,id,buffer);
	}
}

int close_logging(){
	if(is_logging_set==1){
		if(is_log1_set==1){
			fflush(file1);
			fclose(file1);
			is_log1_set=0;
		}
		if(is_log2_set==1){
			fflush(file2);
			fclose(file2);
			is_log2_set=0;
		}
		if(is_log3_set==1){
			fflush(file3);	
			fclose(file3);
			is_log3_set=0;
		}
		if(is_log4_set==1){
			fflush(file4);
			fclose(file4);
			is_log4_set=0;
		}
		is_logging_set=-1;
	}
}

//--------------File------------------

//-----------Serial-------------------------

int init_serial(){
	if(is_serial_set!=1){
		output("Trying to init serial");
		serial1=open(SERIALDEVICE1, O_RDWR | O_NOCTTY );
		serial2=open(SERIALDEVICE2, O_RDWR | O_NOCTTY );
		serial3=open(SERIALDEVICE3, O_RDWR | O_NOCTTY );
		serial4=open(SERIALDEVICE4, O_RDWR | O_NOCTTY );
		if (serial1 <0) {output("Error with SERIALDEVICE1"); return -1; }
		if (serial2 <0) {output("Error with SERIALDEVICE2"); return -1; }
		if (serial3 <0) {output("Error with SERIALDEVICE3"); return -1; }
		if (serial4 <0) {output("Error with SERIALDEVICE4"); return -1; }
		tcgetattr(serial1,&oldtio1);
		tcgetattr(serial2,&oldtio2);
		tcgetattr(serial3,&oldtio3);
		tcgetattr(serial4,&oldtio4);
		bzero(&newtio, sizeof(newtio));

		newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
		newtio.c_iflag = IGNPAR | ICRNL;
		newtio.c_oflag = 0;
		newtio.c_lflag = ICANON;
		newtio.c_cc[VINTR]    = 0;
		newtio.c_cc[VQUIT]    = 0;
		newtio.c_cc[VERASE]   = 0;
		newtio.c_cc[VKILL]    = 0;
		newtio.c_cc[VEOF]     = 4;
		newtio.c_cc[VTIME]    = 0;
		newtio.c_cc[VMIN]     = 1;    //CHANGED
		newtio.c_cc[VSWTC]    = 0;
		newtio.c_cc[VSTART]   = 0;
		newtio.c_cc[VSTOP]    = 0;
		newtio.c_cc[VSUSP]    = 0;
		newtio.c_cc[VEOL]     = 0;
		newtio.c_cc[VREPRINT] = 0;
		newtio.c_cc[VDISCARD] = 0;
		newtio.c_cc[VWERASE]  = 0;
		newtio.c_cc[VLNEXT]   = 0;
		newtio.c_cc[VEOL2]    = 0;
      
		output("Trying to flush serial");
		
		tcflush(serial1, TCIFLUSH);
		tcflush(serial2, TCIFLUSH);
		tcflush(serial3, TCIFLUSH);
		tcflush(serial4, TCIFLUSH);

		output("Setting attrs");
		
		tcsetattr(serial1,TCSANOW,&newtio);
		tcsetattr(serial2,TCSANOW,&newtio);
		tcsetattr(serial3,TCSANOW,&newtio);
		tcsetattr(serial4,TCSANOW,&newtio);
		
		output("Starting other stuff");
		
		if(switch_serials()==1 && set_additional_serial_functions()==1){
			output("Init serial: success");
			return 1;
		}
		else{
			output("Init serial: fail");
			return -1;
		}
	}
	else{
		return -1;
	}
}

char get_id_from_serial(int serial){
	char read_buffer[10];
	memset(read_buffer,'\0',10);
	int char_write=write(serial,"?\n",2);
	int char_read=read(serial,read_buffer,10);
	if(read_buffer[2]=='7'){
		return read_buffer[0];
	}
	else{
		return '0';
	}
}

int set_has_ball_from_serial(){
	char read_buffer[10];
	int char_write=write(serial_has_ball,"gb\n",3);
	int char_read=read(serial_has_ball,read_buffer,10);
	if(read_buffer[2]=='2' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_QUERY_HAS_BALL)){
		has_ball=read_buffer[4]-'0';
	}
}

int set_kick_coilgun(int i){
	char write_buffer[6];
	sprintf(write_buffer,"k%i\n",i);
	write(serial_coilgun,write_buffer,strlen(write_buffer));
}

int set_ping_coilgun(){
	write(serial_coilgun,"p\n",2);
}

int set_discharge_coilgun(){
	write(serial_coilgun,"d\n",2);
}

int set_failsafe_discharge_coilgun(int i){
	char write_buffer[3];
	sprintf(write_buffer,"k%i\n",i);
	write(serial_coilgun,write_buffer,strlen(write_buffer));
}

int set_charge_coilgun(){
	write(serial_coilgun,"c\n",2);
}

int set_dribbler(int speed){
	char buf[6];
	if(speed>=0 && speed<17){
		sprintf(buf,"sd%i\n",speed);
		write(serial_coilgun,buf,strlen(buf));
	}
}

int set_which_goal_from_serial(){
	char read_buffer[10];
	int char_write=write(serial_which_goal,"gl\n",3);
	int char_read=read(serial_which_goal,read_buffer,10);
	if(read_buffer[2]=='1' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_QUERY_WHICH_GOAL)){
		which_goal=1+read_buffer[4]-'0';
	}
}

int set_robot_going_from_serial(){
	char read_buffer[10];
	int char_write=write(serial_robot_going,"go\n",3);
	int char_read=read(serial_robot_going,read_buffer,10);
	if(read_buffer[2]=='0' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_QUERY_ROBOT_GOING)){
		robot_going=read_buffer[0]-'0';
	}
}

int init_read_serial(){
	if(is_logging_set==1){
		if(serial_arn>0){
			is_read_serial_arn_set=1;
		}
		is_read_serial_1_set=1;
		is_read_serial_2_set=1;
		is_read_serial_3_set=1;
		return set_read_serial(1);;
	}
	else{
		return -1;
	}
}

void close_read_serial(){
	if(is_serial_read_set==1){
		is_serial_read_set=set_read_serial(-1);
	}
}

int set_read_serial(int i){
	if(i==1 && is_serial_read_set!=1){
		output("Starting read_serial threads");
		int err1=pthread_create(&(tid[0]),NULL,&read_serial_arn,NULL);
		int err2=pthread_create(&(tid[1]),NULL,&read_serial_1,NULL);
		int err3=pthread_create(&(tid[2]),NULL,&read_serial_2,NULL);
		int err4=pthread_create(&(tid[3]),NULL,&read_serial_3,NULL);
		if(err1==0 && err2==0 && err3==0 && err4==0){
			output("	succes");
			return 1;
		}
		else{
			if(err1!=0){
			    output("Read_serial_arn (thread) was not stared");
			}
			if(err2!=0){
			    output("Read_serial_1 (thread) was not stared");
			}
			if(err3!=0){
			    output("Read_serial_2 (thread) was not stared");
			}
			if(err4!=0){
			    output("Read_serial_3 (thread) was not stared");
			}
			output("	fail");
			return -1;
		}
	}
	else if(i!=1 && is_serial_read_set==1){
		char read_buffer[10];
		int char_write;
		int timer_counter;
		output("Ending read_serial threads");
		char_write=write(serial_arn,"ar0\n",4);
		output("Waiting for response about shutting down read_serial_arn, could take up to a second");
		while(is_read_serial_arn_set!=0 && timer_counter<1000){
			usleep(1000);
			timer_counter++;
		}
		if(is_read_serial_arn_set!=0){
			is_read_serial_arn_set=0;
			output("	fail");

		}
		else{
			output("	success");
		}
		is_read_serial_1_set=-1;
		is_read_serial_2_set=-1;
		is_read_serial_3_set=-1;
		
		output("Waiting to see if all files closed normally, could take up to a second");
		timer_counter=0;
		while((is_log1_set==1 || is_log2_set==1 || is_log3_set==1 || is_log4_set==1) && timer_counter<1000){
			usleep(1000);
			timer_counter++;
		}
		if(is_log1_set==1 || is_log2_set==1 || is_log3_set==1 || is_log4_set==1){
			close_logging();
			output("	fail");
		}
		else{
			output("	success");
		}
		return -1;
	}
}

void *read_serial_arn(void *arg){
	pthread_t id = pthread_self();
	char read_buffer[10];
	int char_write=write(serial_arn,"ar1\n",4);
	int char_read=read(serial_arn,read_buffer,10);
	output("read_serial_arn (thread) was started");
	if(char_read==6 && read_buffer[2]=='3' && (char)(((int)'0')+SERIAL_TO_ARN)){
		is_read_serial_arn_set=read_buffer[4]-'0';
		output("read_serial_arn (thread) was started: serial_arn responded correctly");
		//char_write=write(serial_arn,"?\n",2);
		printf("SERIAL TO ARN : %i\n",SERIAL_TO_ARN);
		while(is_read_serial_arn_set==1 && main_going==1){
			memset(read_buffer,'\0',255);
			char_read=read(serial_arn,read_buffer,10);
			output(read_buffer);
			if(char_read==10 && read_buffer[2]=='4' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_ARN)){
				robot_going=read_buffer[4]-'0';
				has_ball=read_buffer[6]-'0';
				which_goal=1+read_buffer[8]-'0';
			}
			else if(char_read==6 && read_buffer[2]=='3' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_ARN)){
				is_read_serial_arn_set=read_buffer[4]-'0';
			}
			else if(char_read==4 && read_buffer[2]=='7' && read_buffer[0]==(char)(((int)'0')+SERIAL_TO_ARN)){
				;
			}
			else if(char_read==0){
				;
				/*close_serial();
				while(is_serial_set!=1){
					is_serial_set=1;
					close_serial();
					usleep(1000000);
					is_serial_set=init_serial();
					
				}
				output("Fixed serial");
				//close();*/	
			}
			//char_write=write(serial_arn,"?\n",2);
			usleep(1);
		}
		output("read_serial_arn (thread) was stopped: successfully");	
		pthread_exit(0);
	}
	else{
		output("read_serial_arn (thread) was stopped: serial_arn response is false");
		pthread_exit(0);
	}
}

void *read_serial_1(void *arg){
	pthread_t id = pthread_self();
	char read_buffer[255];
	int char_read;
	int char_write;
	output("read_serial_1 (thread) was started");
	//char_write=write(serial_1,"?\n",2);
	while(is_read_serial_1_set==1 && is_log1_set==1 && main_going==1){
		memset(read_buffer,'\0',255);
		char_read=read(serial_1,read_buffer,255);
		if(char_read==4 && read_buffer[2]=='7' && read_buffer[0]=='2'){
			usleep(100000);
		}
		else if(char_read==0){
			;//output("CHAR READ WAS ZERO (serial 1), DOING NOTHING");
			//close();
		}
		else{
			log2(2,read_buffer);
		}
		usleep(1);
		//char_write=write(serial_1,"?\n",2);
	}
	is_log1_set==-1;
	output("read_serial_1 (thread) was stopped: successfully");	
	pthread_exit(0);
}

void *read_serial_2(void *arg){
	pthread_t id = pthread_self();
	char read_buffer[255];
	int char_read;
	int char_write;
	output("read_serial_2 (thread) was started");
	//char_write=write(serial_2,"?\n",2);
	while(is_read_serial_2_set==1 && is_log2_set==1 && main_going==1){
		memset(read_buffer,'\0',255);
		char_read=read(serial_2,read_buffer,255);
		if(char_read==4 && read_buffer[2]=='7' && read_buffer[0]=='3'){
			usleep(100000);
		}
		else if(char_read==0){
			;//output("CHAR READ WAS ZERO (serial 2), DOING NOTHING");
			//close();
		}
		else{
			log3(3,read_buffer);
		}
		usleep(1);
		//char_write=write(serial_2,"?\n",2);
	}
	is_log2_set=-1;
	output("read_serial_2 (thread) was stopped: successfully");	
	pthread_exit(0);
}

void *read_serial_3(void *arg){
	pthread_t id = pthread_self();
	char read_buffer[255];
	int char_read;
	int char_write;
	output("read_serial_3 (thread) was started");
	//char_write=write(serial_3,"?\n",2);
	while(is_read_serial_3_set==1 && is_log3_set==1 && main_going==1){
		memset(read_buffer,'\0',255);
		char_read=read(serial_3,read_buffer,255);
		if(char_read==4 && read_buffer[2]=='7' && read_buffer[0]=='4'){
			usleep(100000);
		}
		else if(char_read==0){
			;//output("CHAR READ WAS ZERO (serial 3), DOING NOTHING");
			//close();
		}
		else{
			log4(4,read_buffer);
		}
		
		usleep(1);
		//char_write=write(serial_3,"?\n",2);
	}
	is_log3_set=-1;
	output("read_serial_3 (thread) was stopped: successfully");
	pthread_exit(0);
}

int set_additional_serial_functions(){
	int result;
	switch(SERIAL_TO_QUERY_HAS_BALL){
		case 1:
			serial_has_ball=serial1;
			break;
		case 2:
			serial_has_ball=serial2;
			break;
		case 3:
			serial_has_ball=serial3;
			break;
		case 4:
			serial_has_ball=serial4;
			break;
		default:
			serial_has_ball=-1;
			break;
	}
	switch(SERIAL_TO_QUERY_WHICH_GOAL){
		case 1:
			serial_which_goal=serial1;
			break;
		case 2:
			serial_which_goal=serial2;
			break;
		case 3:
			serial_which_goal=serial3;
			break;
		case 4:
			serial_which_goal=serial4;
			break;
		default:
			serial_which_goal=-1;
			break;
	}
	switch(SERIAL_TO_QUERY_ROBOT_GOING){
		case 1:
			serial_robot_going=serial1;
			break;
		case 2:
			serial_robot_going=serial2;
			break;
		case 3:
			serial_robot_going=serial3;
			break;
		case 4:
			serial_robot_going=serial4;
			break;
		default:
			serial_robot_going=-1;
			break;
	}
	switch(SERIAL_TO_ARN){
		case 1:
			serial_arn=serial1;
			serial_1=serial2;
			serial_2=serial3;
			serial_3=serial4;
			break;
		case 2:
			serial_arn=serial2;
			serial_1=serial1;
			serial_2=serial3;
			serial_3=serial4;
			
			break;
		case 3:
			serial_arn=serial3;
			serial_1=serial1;
			serial_2=serial2;
			serial_3=serial4;
			break;
		case 4:
			serial_arn=serial4;
			serial_1=serial1;
			serial_2=serial2;
			serial_3=serial3;
			break;
		default:
			serial_arn=-1;
			break;
	}
	switch(SERIAL_TO_COILGUN){
		case 1:
			serial_coilgun=serial1;
			break;
		case 2:
			serial_coilgun=serial2;
			break;
		case 3:
			serial_coilgun=serial3;
			break;
		case 4:
			serial_coilgun=serial4;
			break;
		default:
			serial_coilgun=-1;
			break;
	}
	if(serial_has_ball>0 && serial_which_goal>0 && serial_robot_going>0 && serial_arn>0 && serial_coilgun>0){
		output("Additional serials added");
		return 1;
	}
	else{
		return -1;
	}
}

int switch_serials(){
	output("Starting to switch serials");
	char id_serial_tmp1,id_serial_tmp2,id_serial_tmp3,id_serial_tmp4;
	char id_serial1,id_serial2,id_serial3,id_serial4;
	int tmp1,tmp2,tmp3,tmp4;
	int fail=0;
	char buffer[256];
	
	
	/*write(serial1,"?\n",2);
	write(serial2,"?\n",2);
	write(serial3,"?\n",2);
	write(serial4,"?\n",2);*/
	
	
	id_serial_tmp1=get_id_from_serial(serial1);
	id_serial_tmp2=get_id_from_serial(serial2);
	id_serial_tmp3=get_id_from_serial(serial3);
	id_serial_tmp4=get_id_from_serial(serial4);

	id_serial_tmp1=get_id_from_serial(serial1);
	id_serial_tmp2=get_id_from_serial(serial2);
	id_serial_tmp3=get_id_from_serial(serial3);
	id_serial_tmp4=get_id_from_serial(serial4);	
	
	id_serial1=get_id_from_serial(serial1);
	id_serial2=get_id_from_serial(serial2);
	id_serial3=get_id_from_serial(serial3);
	id_serial4=get_id_from_serial(serial4);

	tmp1=serial1;
	tmp2=serial2;
	tmp3=serial3;
	tmp4=serial4;
	sprintf(buffer,"Id-s first reading %c : %c : %c : %c",id_serial1,id_serial2,id_serial3,id_serial4);
	output(buffer);

	if(id_serial1!='1'){
		if(id_serial2=='1'){
			tmp1=serial2;
		}
		else if(id_serial3=='1'){
			tmp1=serial3;
		}
		else if(id_serial4=='1'){
			tmp1=serial4;
		}
		else{
			fail=-1;
		}
	}
	if(id_serial2!='2'){
		if(id_serial1=='2'){
			tmp2=serial1;
		}
		else if(id_serial3=='2'){
			tmp2=serial3;
		}
		else if(id_serial4=='2'){
			tmp2=serial4;
		}
		else{
			fail=-1;
		}
	}
	if(id_serial3!='3'){
		if(id_serial1=='3'){
			tmp3=serial1;
		}
		else if(id_serial2=='3'){
			tmp3=serial2;
		}
		else if(id_serial4=='3'){
			tmp3=serial4;
		}
		else{
			fail=-1;
		}
	}
	if(id_serial4!='4'){
		if(id_serial1=='4'){
			tmp4=serial1;
		}
		else if(id_serial2=='4'){
			tmp4=serial2;
		}
		else if(id_serial3=='4'){
			tmp4=serial3;
		}
		else{
			fail=-1;
		}
	}

	serial1=tmp1;
	serial2=tmp2;
	serial3=tmp3;
	serial4=tmp4;

	tmp1=get_id_from_serial(serial1);
	tmp2=get_id_from_serial(serial2);
	tmp3=get_id_from_serial(serial3);
	tmp4=get_id_from_serial(serial4);

	sprintf(buffer,"Ids second reading %c : %c : %c : %c \n",tmp1,tmp2,tmp3,tmp4);
	output(buffer);

	if(tmp1=='1' && tmp2=='2' && tmp3=='3' && tmp4=='4' && fail==0){
		output("Serials switched: success");
		return 1;
	}
	else {
		output("Serials switched: fail");
		return -1;
	}
}

int close_serial(){
	if(is_serial_set==1){
		tcsetattr(serial1,TCSANOW,&oldtio1);
		tcsetattr(serial2,TCSANOW,&oldtio2);
		tcsetattr(serial3,TCSANOW,&oldtio3);
		tcsetattr(serial4,TCSANOW,&oldtio4);
		close(serial1);
		close(serial2);
		close(serial3);
		close(serial4);
		is_serial_set=-1;
	}
	
}

//-----------------------Serial ----------------

//----------------------Movement----------------

int init_movement(){
	sprintf(command_turn_without_ball_right,"sd-%i\n",TURN_SPEED_WITHOUT_BALL);
	sprintf(command_turn_without_ball_left,"sd%i\n",TURN_SPEED_WITHOUT_BALL);
	sprintf(command_turn_with_ball_right,"sd-%i\n",TURN_SPEED_WITH_BALL);
	sprintf(command_turn_with_ball_left,"sd%i\n",TURN_SPEED_WITH_BALL);

	sprintf(command_turn_with_ball_right2,"sd-%i\n",TURN_SPEED_WITH_BALL2);
	sprintf(command_turn_with_ball_left2,"sd%i\n",TURN_SPEED_WITH_BALL2);

	command_turn_without_ball_right_length=strlen(command_turn_without_ball_right);
	command_turn_without_ball_left_length=strlen(command_turn_without_ball_left);
	command_turn_with_ball_right_length=strlen(command_turn_with_ball_right);
	command_turn_with_ball_left_length=strlen(command_turn_with_ball_left);
	command_turn_with_ball_right_length2=strlen(command_turn_with_ball_right2);
	command_turn_with_ball_left_length2=strlen(command_turn_with_ball_left2);

	output("Movement init: success");
	return 1;
}

int stop(){
	int ia=write(serial1,"sd0\n",4);
	int ib=write(serial2,"sd0\n",4);
	int ic=write(serial3,"sd0\n",4);
}

int forward(double velocity,double angle){

	int ia,ib,ic;

	char command1[10];
	char command2[10];
	char command3[10];
	char command11[10];
	char command22[10];
	char command33[10];

	int velocity1,velocity2,velocity3;

	velocity1=velocity*cos((double)PI*(150-angle)/180);
	velocity2=velocity*cos((double)PI*(30-angle)/180);
	velocity3=velocity*cos((double)PI*(270-angle)/180);

	sprintf(command1,"sd%i\n",velocity1/2);
	sprintf(command2,"sd%i\n",velocity2/2);
	sprintf(command3,"sd%i\n",velocity3/2);

	sprintf(command11,"sd%i\n",velocity1);
	sprintf(command22,"sd%i\n",velocity2);
	sprintf(command33,"sd%i\n",velocity3);

	ia=write(serial1,command1,strlen(command1));
	ib=write(serial2,command2,strlen(command2));
	ic=write(serial3,command3,strlen(command3));

	ia=write(serial1,command11,strlen(command11));
	ib=write(serial2,command22,strlen(command22));
	ic=write(serial3,command33,strlen(command33));
}

double calculate_angle_modifier(int y){
	return 80-40/(double)(pow(3.0,(-1+440.0/(double)y)));
}

double calculate_angle(int x){
	return (WIDTH/2-x)/(WIDTH/2+0.0000000000001);
}

double calculate_speed(int y){
	return 120-85/(double)(pow(3.0,(-1+440.0/(double)y)));
}

int turn_without_ball(int i){
	int ia,ib,ic;
	if(i<0){
		ia=write(serial1,command_turn_without_ball_left,command_turn_without_ball_left_length);
		ib=write(serial2,command_turn_without_ball_left,command_turn_without_ball_left_length);
		ic=write(serial3,command_turn_without_ball_left,command_turn_without_ball_left_length);
	}
	else if(i>=0){
		ia=write(serial1,command_turn_without_ball_right,command_turn_without_ball_right_length);
		ib=write(serial2,command_turn_without_ball_right,command_turn_without_ball_right_length);
		ic=write(serial3,command_turn_without_ball_right,command_turn_without_ball_right_length);
	}
}

int turn_with_ball(int i){
	int ia,ib,ic;
	if(i<0){
		ia=write(serial1,command_turn_with_ball_left,command_turn_with_ball_left_length);
		ib=write(serial2,command_turn_with_ball_left,command_turn_with_ball_left_length);
		ic=write(serial3,command_turn_with_ball_left,command_turn_with_ball_left_length);
	}
	else if(i>=0){
		ia=write(serial1,command_turn_with_ball_right,command_turn_with_ball_right_length);
		ib=write(serial2,command_turn_with_ball_right,command_turn_with_ball_right_length);
		ic=write(serial3,command_turn_with_ball_right,command_turn_with_ball_right_length);
	}
}

int turn_with_ball2(int i){
	int ia,ib,ic;
	if(i<0){
		ia=write(serial1,command_turn_with_ball_left2,command_turn_with_ball_left_length2);
		ib=write(serial2,command_turn_with_ball_left2,command_turn_with_ball_left_length2);
		ic=write(serial3,command_turn_with_ball_left2,command_turn_with_ball_left_length2);
	}
	else if(i>=0){
		ia=write(serial1,command_turn_with_ball_right2,command_turn_with_ball_right_length2);
		ib=write(serial2,command_turn_with_ball_right2,command_turn_with_ball_right_length2);
		ic=write(serial3,command_turn_with_ball_right2,command_turn_with_ball_right_length2);
	}
}

int turn_with_ball3(int i,double speed,double cof1,double cof2){
	int ia,ib,ic;
	double speed1,speed2,speed3;
	char buf1[100];
	char buf2[100];
	char buf3[100];
	char buf4[100];
	char buf5[100];
	char buf6[100];

	speed1=(double)speed*(double)cof1;
	speed2=(double)speed*(double)cof2;

	
	if(i<0){	
	      	sprintf(buf1,"sd%i\n",(int)speed);
		sprintf(buf2,"sd%i\n",(int)speed1);
		sprintf(buf3,"sd%i\n",(int)speed2);
		
	      	sprintf(buf4,"sd%i\n",(int)(speed*0.5));
		sprintf(buf5,"sd%i\n",(int)(speed1*0.5));
		sprintf(buf6,"sd%i\n",(int)(speed2*0.5));


		
		ia=write(serial1,buf5,strlen(buf5));
		ib=write(serial2,buf6,strlen(buf6));
		ic=write(serial3,buf4,strlen(buf4));
		
		ia=write(serial1,buf2,strlen(buf2));
		ib=write(serial2,buf3,strlen(buf3));
		ic=write(serial3,buf1,strlen(buf1));
	}
	else if(i>=0){
		
	      	sprintf(buf1,"sd-%i\n",(int)speed);
		sprintf(buf2,"sd-%i\n",(int)speed1);
		sprintf(buf3,"sd-%i\n",(int)speed2);
		
	      	sprintf(buf4,"sd-%i\n",(int)(speed*0.5));
		sprintf(buf5,"sd-%i\n",(int)(speed1*0.5));
		sprintf(buf6,"sd-%i\n",(int)(speed2*0.5));
		
		
		ia=write(serial1,buf6,strlen(buf6));
		ib=write(serial2,buf5,strlen(buf5));
		ic=write(serial3,buf4,strlen(buf4));
		
		ia=write(serial1,buf3,strlen(buf3));
		ib=write(serial2,buf2,strlen(buf2));
		ic=write(serial3,buf1,strlen(buf1));		
	}
}

//----------------------Movement---------------------

//----------------------GUI-----------------------
int init_gui(){
	if(is_gui_set!=1){
		cvNamedWindow("Robot", CV_WINDOW_AUTOSIZE);
		rgbimg = cvCreateImage(cvSize(WIDTH, HEIGHT), IPL_DEPTH_8U, 3);
		yuvimg = cvCreateImage(cvSize(WIDTH, HEIGHT), IPL_DEPTH_8U, 2);
		output("Gui init: success");
		return 1;
	}
	else{
		return -1;
	}
}

void show_image(char *buf, int isrgb){
	if (isrgb){	
		rgbimg->imageData = buf;
	}
	else{
		yuvimg->imageData = buf;
		cvCvtColor(yuvimg, rgbimg, CV_YUV2BGR_YUYV);
	}
}
 
void show_blank(){
	cvZero(rgbimg);
}

void draw_object(int x1, int y1, int x2, int y2){
	cvRectangle(rgbimg, cvPoint(x1, y1), cvPoint(x2, y2), cvScalar(255, 0, 0, 0), 3, 8, 0);
}

void show_gui(){
	cvShowImage("Robot", rgbimg);
}

void save_image(){
	char filename[50];
	int attributes[2];
	attributes[0]=CV_IMWRITE_PNG_COMPRESSION;
	attributes[1]=0;

	IplImage* rgbimg_tmp=rgbimg;
	time_t now=time(0);
	tm *ltm=localtime(&now);
	sprintf(filename, "./pictures/%i.%i.%i",1900+ltm->tm_year,ltm->tm_mon,ltm->tm_mday);
	mkdir(filename,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	sprintf(filename, "./pictures/%i.%i.%i/img%d.png",1900+ltm->tm_year,ltm->tm_mon,ltm->tm_mday,imgcount++);
	cvSaveImage(filename, rgbimg_tmp,attributes);
}

void close_gui(){
	if(is_gui_set==1){
		is_gui_set=-1;
	}
}

int gui(){

	if(init(GUI)!=1){
		output("Init failed");
		close();
		return -1;
	}
	else{
		output("Started GUI");
		char c;
		int status=0;
		int x,y,i;
		int ball_status;
		char buffer[256];
		
		while(1 && is_gui_set==1){
			process_frame();
			if(status==0){
				show_image((char *) vision.buf,0);
			}
			else{
				vision.generateThresholdImage();
				show_image((char *) vision.bufthresh,1);
			}

			show_gui();
			c=cvWaitKey(1);

			if(c=='1'){
				whitbalance+=50;
				sprintf(buffer,"Whitebalance %i",whitbalance);
				output(buffer);
				capture.vid.setControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, whitbalance);
			}
			if(c=='2'){
				whitbalance-=50;
				sprintf(buffer,"Whitebalance %i",whitbalance);
				output(buffer);
				capture.vid.setControl(V4L2_CID_WHITE_BALANCE_TEMPERATURE, whitbalance);
			}
			if(c=='3'){
				saturation+=5;
				sprintf(buffer,"Saturation %i",saturation);
				output(buffer);
				capture.vid.setControl(V4L2_CID_SATURATION, saturation);
			}
			if(c=='4'){
				saturation-=5;
				sprintf(buffer,"Saturation %i",saturation);
				output(buffer);
				capture.vid.setControl(V4L2_CID_SATURATION, saturation);
			}
			if(c=='5'){
				exposure+=5;
				sprintf(buffer,"Exposure %i",exposure);
				output(buffer);
				capture.vid.setControl(V4L2_CID_EXPOSURE_ABSOLUTE, exposure);
			}
			if(c=='6'){
				exposure-=5;
				sprintf(buffer,"Exposure %i",exposure);
				output(buffer);
				capture.vid.setControl(V4L2_CID_EXPOSURE_ABSOLUTE, exposure);
			}
			if(c=='7'){
				gain+=5;
				sprintf(buffer,"Gain %i",gain);
				output(buffer);
				capture.vid.setControl(V4L2_CID_GAIN, gain);
			}
			if(c=='8'){
				gain-=5;
				sprintf(buffer,"Gain %i",gain);
				output(buffer);
				capture.vid.setControl(V4L2_CID_GAIN, gain);
			}
			if(c=='s'){
				output("saved image");
				save_image();
			}
			if(c=='q'){
				break;
			}
			if(c=='n'){
				status=0;
			}
			if(c=='t'){
				status=1;
			}
			if(c=='b'){
				printf("Getting ball info");
				int x,y,e;
				e=get_ball(x,y);
				sprintf(buffer,"Ball: %i (%i:%i)",e,x,y);
				output(buffer);
			}
			if(c=='e'){
				int e;
				e=check_edge();
				sprintf(buffer,"Edge: %i",e);
				output(buffer);
			}
			if(c=='p'){
				check_ball_in_way(320,200);
			}
			if(c=='o'){
				which_goal=1;
				goal=vision.getRegions(which_goal+2);	
				if(goal && goal->area >450){
					int x1=goal->x1;
					int x2=goal->x2;
					int y1=goal->y1;
					int y2=goal->y2;
					int area=goal->area;
					int x=-WIDTH/2+(x1+x2)/2;
					int y=(y1+y2)/2;
					printf("INFO x1:%i; x2:%i; y1:%i; y2:%i; x:%i; y:%i; area:%i\n",x1,x2,y1,y2,x,y,area);
					if(area>2000){
						if(x>-7 && x<7){
							if(check_ball_in_way(x,y)==1){
								output("Kicking");
							}
							else{
								if(x2>320){
									output("Something in way, going right");
								}
								else if(x1<320){
									output("Something in way,going left");
								}
							}	
						}
						else if(x<=-7){
							printf("Aiming left %i %i\n",x,y);
						}
						else if(x>=7){
							printf("Aiming right %i %i\n",x,y);
						}
					}
					else{
					
						int check1=check_ball(200,240);
						int check2=check_ball(440,240);
						if(check1==1 && check2!=1){
							output("making angle left");
						}
						else if(check1!=1 && check2==1){
							output("making angle right");
						}
						else if(check1!=1){
							output("Error kick");
						}
						else{
							  if(x1>320){
								output("Going left for sure");
							  }
							  else if(x2<320){
								output("going right for sure");
							  }
							  else{
								output("Going left for fun");
							  }
						}
					}
				}
				else{
					printf("Dont see goal, starting to search\n");
				}
			}
			if(c=='y'){
				goal=vision.getRegions(which_goal);
				if(goal && goal->area >200){
					int x1,x2,y1,y2;
					x1=goal->x1;
					x2=goal->x2;
					y1=goal->y1;
					y2=goal->y2;
					int x=-WIDTH/2+(goal->x1+goal->x2)/2;
					int y=(goal->y1+goal->y2)/2;
					sprintf(buffer,"goal: (yellow) (%i:%i:%i:%i)",x1,x2,y1,y2);
					output(buffer);
				}
			}
			if(c=='v'){
				goal=vision.getRegions(blue-1);
				if(goal && goal->area >200){
					int x=-WIDTH/2+(goal->x1+goal->x2)/2;
					int y=(goal->y1+goal->y2)/2;
					sprintf(buffer,"goal: (blue) (%i:%i)",x,y);
					output(buffer);
				}
			}
		}
		output("Ending: gui");
		close();
		return 1;
	}
}

//---------------------GUI--------------------

//----------------------Vision--------------------

void process_frame(){
	const Capture::Image *img = capture.captureFrame();
	bool result=false;
	if(img != NULL){
		vision_image cmv_img;
		cmv_img.buf    = (pixel*)(img->data);
		cmv_img.width  = img->width;
		cmv_img.height = img->height;
		cmv_img.pitch  = img->bytesperline;
		cmv_img.field  = img->field;

		vision.processFrame(cmv_img);
		capture.releaseFrame(img);
	}
	else{
		
		output("Camera Error");
		output("Trying to close camera");
		
		is_vision_set=-1;
		capture.close();
		output("Closed vision: success");
		while(result!=true){
			usleep(100000);
			result=capture.init(VIDEODEVICE, WIDTH, HEIGHT, V4L2_PIX_FMT_YUYV);
		}
		is_vision_set=1;
		output("fixed");
		
	}
}

int init_vision(){
	if(is_vision_set!=1){
		output("Capture_init check:");
		if(capture.init(VIDEODEVICE, WIDTH, HEIGHT, V4L2_PIX_FMT_YUYV)!=true){
			output("	fail");
			return -1;
		}
		output("	success");
		output("Vision_init check:");
		if(vision.init(COLORFILE, TMAPFILE, WIDTH, HEIGHT)!=true){
			output("	fail");
			return -1;
		}
		output("	success");
		capture.vid.setControl(V4L2_CID_HFLIP, 1);
		capture.vid.setControl(V4L2_CID_VFLIP, 1);
		capture.vid.setControl(V4L2_CID_POWER_LINE_FREQUENCY, 1);

		return 1;
	}
	else{
		return -1;
	}
}

void close_vision(){
	if(is_vision_set==1){
		capture.close();
		vision.close();
		is_vision_set=-1;
	}
}

void start_find_ball(){
	if(find_ball_set=-1){
		int err1=pthread_create(&(tid[4]),NULL,&find_ball,NULL);
	}
}

void *find_ball(void *arg){
	pthread_t id=pthread_self();
	int forward_count=0;
	int turn_ball_count=0;
	int i;
	find_ball_set=-1;
	find_goal_set=-1;
	output("Created find ball thread");

	while(robot_going==1){
		forward_count=0;

		while(robot_going==1 && find_ball_set==0){
			//output("Finding ball inner loop");
			if(just_started==1 && find_ball_set==0){
				i=0;
				while(just_started==1 && find_ball_set==0 && i<1600){
					output("started");
					forward(93,0);
					usleep(2500);
					i++;
				}
				just_started=0;
			}
			if(forward_count==0){
				//output("Finding ball inner loop forward_count==0");
				i=0;
				while(find_ball_set==0 && i<400){
					turn_without_ball(1);
					usleep(2500);//SHOULD TURN 180 degrees
					i++;
				}
				forward_count=1;
			}
			else{
				if(check_edge()==1 && forward_count<2000){//SHOULD DRIVE AHEAD FOR ONE METER
					forward(80,0);
					usleep(2500);
					forward_count++;
				}
				else{
					if(forward_count>1999){
						output("Finding ball inner loop edge detected");
						i=0;
						while(find_ball_set==0 && i<50){
							turn_without_ball(1);
							usleep(2500);//SHOULD TURN 45 degrees
							i++;
						}
						forward_count=1;
					}
					else{					
						i=0;
						while(find_ball_set==0 && i<50){
							turn_without_ball(1);
							usleep(2500);//SHOULD TURN 90 degrees
							i++;
						}
						forward_count=1;
					}
				}
			}
		}
		turn_ball_count=0;
		while(robot_going==1 && find_goal_set==0){
			//TO IMPLEMENT CORRECTLY//
			if(turn_ball_count==0){
				i=0;
				while(find_goal_set==0 && i<1040){
					turn_with_ball3(side_to_search_goal,70,0.35,0.1);
					usleep(2500);
					i++;
				}
				turn_ball_count=1;
			}
			else{
				i=0;
				turn_ball_count=0;
				while(find_goal_set==0 && i<500 && check_edge()==1){
					forward(40,0);
					usleep(2500);
					i++;
				}
				turn_ball_count=0;
			}
		}
	}
	find_ball_set=-1;
	find_goal_set=-1;
	output("Find_ball thread: over");
	pthread_exit(0);
}

int check_edge(){
	double x_val=320;
	int y_val=220;
	int black_count=1;
	int white_count=0;
	int unknown_count=0;
	int known_count=1;
	int order_test=0;
	char buf[100];
	double deriative;
	deriative=(double)(320-x_val)/(double)(440.01-y_val);
	while(y_val<440){
		if(is_vision_set!=1){
			return -1;
		}
		checkreg=vision.findRegion((int)x_val,(int)y_val);
		
		if(checkreg && checkreg->color==black){
			black_count++;

		}
		else if(checkreg && checkreg->color==white){
			white_count++;
		}
		else if(checkreg && checkreg->color==green){
			known_count++;
		}
		else{
			unknown_count++;
		}
		  
		if(order_test==0 && black_count>15 && white_count<5){
			order_test=1;
		}

		
		if((double)(abs(white_count-black_count))/(double)black_count<0.97 && order_test==1){		
			return -1;
		}
		y_val+=1;
		x_val+=deriative;
	}
	if((double)black_count/(double)(y_val-220)<0.2 && black_count<35){
		return 1;	
	}
	else{
		return -1;
	}

}

int check_ball_in_way(int x,int y){
	return 1;
	double x_val1=x-4;
	int y_val=y+50;
	double x_val2=x+4;
	double deriative1;
	double deriative2;
	
	deriative1=(double)(320-x_val1)/(double)(440.01-y_val);
	deriative2=(double)(320-x_val2)/(double)(440.01-y_val);
	
	int good_value=0;
	int bad_value=1;
	int black_value=0;
	while(y_val<440){
		if(is_vision_set!=1){
			return -1;
		}
		if(y_val%2==0){
			checkreg=vision.findRegion((int)x_val1,(int)y_val);
		}
		else{
			checkreg=vision.findRegion((int)x_val2,(int)y_val);
		}
		if(checkreg && (checkreg->color==green || checkreg->color==white)){
			good_value++;
		}
		else if(checkreg && checkreg->color==black){
			black_value++;
		}
		else{
			bad_value++;
		}
		if(((double)bad_value/(double)(good_value+black_value)>0.05 && bad_value>10) || black_value>20){
			printf("returned -1 (%i,%i) ; bad value %i, good value %i, black value %i\n",x,y,bad_value,good_value,black_value);
			return -1;
		}
		y_val+=3;
		x_val1+=deriative1;
		x_val2+=deriative2;
	}
	printf("returned 1(%i,%i); bad value %i, good value %i, black value %i\n",x,y,bad_value,good_value,black_value);
	return 1;
	      
}

int get_ball(int &x,int &y){
	if(is_vision_set!=1){
		return -1;
	}
	Ball=vision.getRegions(ball);
	while(Ball){
		if(Ball->area >40){
			x=(Ball->x1+Ball->x2)/2;
			y=(Ball->y1+Ball->y2)/2;
			if(check_ball(x,y)==1){
				return 1;
			}
			else{
				Ball=Ball->next;
			}
		}
		else{
			break;//Ball=Ball->next;
		}
	}
	return 0;
}

int check_ball(int x,int y){
	double x_val;
	int y_val=y;
	int white_count=0;
	int black_count=1;
	int unknown_count;
	y_val=y;
	x_val=(double)x;
	char buf[100];
	double deriative;
	double result;
	int order_test=0;
	deriative=(double)(320-x_val)/(double)(400.01-y_val);
	while(y_val<400){
		if(is_vision_set!=1){
			return -1;
		}
		checkreg=vision.findRegion((int)x_val,(int)y_val);
		if(checkreg && checkreg->color==black){
			black_count++;
		}
		else if(checkreg && checkreg->color==white){
			white_count++;
		}
		else if(checkreg && checkreg->color!=green){
			unknown_count++;
		}
		if(order_test==0 && black_count>15 && white_count<5){
			order_test=1;
		}
		if((double)(abs(white_count-black_count))/(double)black_count<0.97 && order_test==1){
			//printf("check ball (%i,%i),failed,main test (black count %i, white_count %i, order_test %i)\n",x,y,black_count,white_count,order_test);
			return -1;
		}
		y_val+=1;
		x_val+=deriative;
	}
	if(((double)black_count/(double)(1+y_val-y)<0.2 && black_count<35) || y>=400){
		//printf("check ball (%i,%i),success, (black count %i, white_count %i, order_test %i)\n",x,y,black_count,white_count,order_test);
		return 1;
	}
	else{
		//printf("check ball (%i,%i),failed, black count\n",x,y);
		return -1;
	}
}

int check_goal_black_area(){

	goal_black_area=vision.getRegions(black);
	if(goal_black_area->area>50000){
		printf("Goal black area %i\n",goal_black_area->area);
		return -1;
	}
	return 1;
}

void close_robot(){
	if(is_robot_set==1){
		is_robot_set=-1;
	}
}

int start_robot(){
	if(is_robot_set!=1){
		output("Starting robot thread: ");
		int err=pthread_create(&(tid[5]),NULL,&robot,NULL);
		if(err!=0){
			output("	fail");
			return -1;
		}
		else{
			output("	success");
			return 1;
		}
	}
}

void *robot(void *arg){
	int x,y,x1,x2,y1,y2,area;
	double speed;
	output("Robot going");
	double angle;
	double modifier;
	find_ball_set=1;
	find_goal_set=1;
	int i;
	int counter;
	int be_careful;
	set_dribbler(16);
	just_started=1;
	int check1;
	int check2;
	while(is_robot_set==1 && robot_going==1){
		usleep(1);
		process_frame();
		set_ping_coilgun();
		set_charge_coilgun();
		if(has_ball==0){
			//set_dribbler(16);
			find_goal_set=1;
			if(get_ball(x,y)==1){
				just_started=0;
				be_careful=0;
				find_ball_set=1;
				speed=calculate_speed(y);
				modifier=calculate_angle_modifier(y);
				angle=calculate_angle(x);
				
				
				
				if(y>350 && abs(modifier*angle)>2){
				
					if(angle>0){
						forward(40,90);
						usleep(2500);
					
					}
					else{
						forward(40,270);
						usleep(2500);
						
					}
				}
				else{
					forward(speed,angle*modifier);
				}
				
			}
			else{
				find_ball_set=0;			
			}
		}
		else if(has_ball==1){
			set_dribbler(16);
			output("searching goal");
			find_ball_set=1;
			if(is_vision_set==1){
				goal=vision.getRegions(which_goal+2);	
				if(goal && goal->area >450){
					x1=goal->x1;
					x2=goal->x2;
					y1=goal->y1;
					y2=goal->y2;
					area=goal->area;
					x=-WIDTH/2+(x1+x2)/2;
					y=(y1+y2)/2;
					find_goal_set=1;
					check_goal_black_area();
					
					if(check_goal_black_area()==1){
						
						if(x>-7 && x<7){
							
							if(check_ball_in_way(x,y)==1){
								set_kick_coilgun(5000);
							}
							else{
								forward(40,90);
								usleep(200000);
							}	
						}
						else if(x<=-7){
							turn_with_ball3(-1,40,0.35,0.1);
						}
						else if(x>=7){
							turn_with_ball3(1,40,0.35,0.1);
						}
					}
					else{
						check1=check_ball(200,240);
						check2=check_ball(440,240);
						if(check1==1 && check2!=1){
							forward(40,90);
							usleep(50000);
						}
						else if(check1!=1 && check2==1){
							forward(40,270);
							usleep(50000);
						}
						else if(check1!=1){
							if(x>-7 && x<7){
								if(check_ball_in_way(x,y)==1){
									set_kick_coilgun(5000);
								}
								else{
									forward(40,90);
									usleep(200000);
								}	
							}
							
							else if(x<=-7){
								if(x>-14){
									turn_with_ball3(-1,30,0.35,0.1);
								}
								else{
									turn_with_ball3(-1,40,0.35,0.1);
								}
							}
							else if(x>=7){
								if(x<14){
									turn_with_ball3(-1,30,0.35,0.1);
								}
								else{
									turn_with_ball3(1,40,0.35,0.1);
								}
							}
						}
						else{
							output("Aiming should not belowe");
							  if(x1>320){
							      output("going left,goal on right");
								forward(40,90);
								usleep(50000);
							  }
							  else if(x2<320){
								output("going right,goal on left");
								forward(40,270);
								usleep(50000);
							  }
							  else{
								if(x>-7 && x<7){
									if(check_ball_in_way(x,y)==1){
										set_kick_coilgun(5000);
									}
									else{
										forward(40,90);
										usleep(200000);
									}	
								}
								else if(x<=-7){
									turn_with_ball3(-1,40,0.35,0.1);
								}
								else if(x>=7){
									turn_with_ball3(1,40,0.35,0.1);
								}								
							  }
						}
					}
				}
				else{
					find_goal_set=0;
				}
			}
		}
	}
	set_dribbler(0);
	close_robot();
	output("Robot not going");
	pthread_exit(0);
}

//-----------------------Vision--------------------

//-----------------------Networking----------------

int init_socket(){

	struct hostent *hp;
	char buffer[256];
	sock= socket(AF_INET, SOCK_DGRAM, 0);

	if (sock < 0){
		output("init_socket: failed- socket error");
		return -1;
	}
	else{
		server.sin_family = AF_INET;
		hp = gethostbyname(SERVER);
		if (hp==0){
			output("init_socket: failed- unknown host");
			return -1;
		}
		else{
			length=sizeof(struct sockaddr_in);
			bcopy((char *)hp->h_addr,(char *)&server.sin_addr,hp->h_length);
			server.sin_port = htons(PORT);
			send_data("init_socket: success");
			output("init_socket: success");
			return 1;
		}
	}
}

int send_data(char *buffer){
	int send_char;
	send_char=sendto(sock,buffer,strlen(buffer),0,(const struct sockaddr *)&server,length);
	if (send_char < 0){
		is_socket_set=-1;
		output("Error with sending");
		close();
	}
	return send_char;
}

int send_image(){
	char *frame_buffer;
	frame_buffer=(char*)malloc(rgbimg->imageSize);
	memcpy(frame_buffer,rgbimg->imageData,rgbimg->imageSize);

	int i=0;
	int revs=1+strlen(frame_buffer)/1023;
	char *send_frame=(char*)malloc(1024);
	char start_command[100];
	char end_command[100];
	sprintf(start_command,"frs-%i\n",revs);
	sprintf(end_command,"fre-%i\n",revs);
	send_data(start_command);
	for(i;i<revs;i++){
		memcpy(send_frame,frame_buffer+i*1023,1023);
		send_frame[1023]='\0';
		send_data(send_frame);   
	}
	send_data(end_command);
	free(frame_buffer);
}

int recv_data(){
	int recv_char;
	char buffer[1024];
	char output_buffer[1024];
	recv_char=recvfrom(sock,buffer,1024,0,(struct sockaddr *)&from, &length);
	if (recv_char<0){
		is_socket_set=-1;
		output("Recv data corrupted");
		close();
	}
	else{
		if(strcmp(buffer,"fw")==0){
			forward(45.0,0.0);
		}
		if(strcmp(buffer,"bw")==0){
			forward(45.0,180.0);
		}
		if(strcmp(buffer,"tl")==0){
			turn_without_ball(-1);
		}
		if(strcmp(buffer,"tr")==0){
			turn_without_ball(1);
		}
		if(strcmp(buffer,"rd")==0){
			forward(45.0,225.0);
		}
		if(strcmp(buffer,"ru")==0){
			forward(45.0,270.0);
		}
		if(strcmp(buffer,"ld")==0){
			forward(45.0,135.0);
		}
		if(strcmp(buffer,"lu")==0){
			forward(45.0,75.0);
		}
		if(strcmp(buffer,"r180")==0){
			turn_without_ball(1);
			usleep(1000000);
			stop();
		}
		if(strcmp(buffer,"r90")==0){
			turn_without_ball(1);
			usleep(500000);
			stop();
		}
		if(strcmp(buffer,"r45")==0){
			turn_without_ball(1);
			usleep(250000);
			stop();
		}
		if(strcmp(buffer,"l180")==0){
			turn_with_ball(1);
			usleep(1320000);
			stop();
		}
		if(strcmp(buffer,"l90")==0){
			turn_with_ball(1);
			usleep(650000);
			stop();
		}
		if(strcmp(buffer,"l45")==0){
			turn_with_ball(1);
			usleep(325000);
			stop();
		}		
		if(strcmp(buffer,"st")==0){
			stop();
		}
		if(strcmp(buffer,"quit")==0){
			close();
		}
		if(strcmp(buffer,"s")==0){
			output("save image");
			save_image();
		}
		if(strcmp(buffer,"kick")==0){
			set_kick_coilgun(5000);
		}
		if(strcmp(buffer,"ball")==0){
			int x,y,e;
			char buffer[100];
			e=get_ball(x,y);
			sprintf(buffer,"ball: %i (%i:%i",e,x,y);
			output(buffer);
		}
		if(strcmp(buffer,"edge")==0){
			int e;
			char buffer[100];
			e=check_edge();
			sprintf(buffer,"edge-> %i",e);
			output(buffer);
		}
		if(strcmp(buffer,"ct")==0){
			turn_with_ball3(1,70,0.35,0.1);
		}
		if(strncmp(buffer,"calm:",5)==0){
			char calm_buf[100];
			if(strlen(buffer)==6){
				sprintf(calm_buf,"Modifier= %f",calculate_angle_modifier(char_to_int(buffer[5])));
				output(calm_buf);
			}
			else if(strlen(buffer)==7){
				sprintf(calm_buf,"Modifier= %f",calculate_angle_modifier(10*char_to_int(buffer[5])+char_to_int(buffer[6])));
				output(calm_buf);				
			}
			else if(strlen(buffer)==8){
				sprintf(calm_buf,"Modifier= %f",calculate_angle_modifier(100*char_to_int(buffer[5])+10*char_to_int(buffer[6])+char_to_int(buffer[7])));
				output(calm_buf);
			}
		}
		if(strncmp(buffer,"cals:",5)==0){
			char calm_buf[100];
			if(strlen(buffer)==6){
				sprintf(calm_buf,"Modifier= %f",calculate_speed(char_to_int(buffer[5])));
				output(calm_buf);
			}
			else if(strlen(buffer)==7){
				sprintf(calm_buf,"Modifier= %f",calculate_speed(10*char_to_int(buffer[5])+char_to_int(buffer[6])));
				output(calm_buf);				
			}
			else if(strlen(buffer)==8){
				sprintf(calm_buf,"Modifier= %f",calculate_speed(100*char_to_int(buffer[5])+10*char_to_int(buffer[6])+char_to_int(buffer[7])));
				output(calm_buf);
			}
		}

		if(strcmp(buffer,"tc")==0){
			turn_with_ball3(-1,70,0.35,0.1);
		}
		if(strcmp(buffer,"ygo")==0){
			goal=vision.getRegions(which_goal+2);
			char buffer[100];
			sprintf(buffer,"x1: %i x2: %i y1: %i y2: %i, area %i",goal->x1,goal->x2,goal->y1,goal->y2,goal->area);
			output(buffer);
		}
		if(strcmp(buffer,"find")==0){
			output("find");
			find_ball_set=-1;;
			find_goal_set=-1;
			robot_going=1;
			start_find_ball();
		}
		if(strcmp(buffer,"ff")==0){
			find_ball_set=0;
			output("ff");
		}		
		if(strcmp(buffer,"uf")==0){
			find_ball_set=-1;
			output("uf");
		}
		if(strcmp(buffer,"go")==0){
			robot_going=1;
			start_find_ball();
			start_robot();
		}
		if(strcmp(buffer,"end")==0){
			stop();
			robot_going=-1;
		}
		
		if(strcmp(buffer,"rob0")==0){
			robot_going=0;
		}
		if(strcmp(buffer,"rob1")==0){
			robot_going=1;
		}
		if(strcmp(buffer,"db1")==0){
			set_dribbler(16);
		}
		if(strcmp(buffer,"db0")==0){
			set_dribbler(0);
		}
		if(strcmp(buffer,"info")==0){
			sprintf(output_buffer,"info\n:  ball: %i\n  goal: %i\n  rob: %i",has_ball,which_goal,robot_going);
			output(output_buffer);
		}
		memset(buffer,'\0',1024);

	}
	return recv_char;
}

void * recieving_data(void *arg){
	pthread_t id = pthread_self();
	int recv_char;
	while(main_going==1 && is_socket_set==1 && sock>=0){
		recv_char=recv_data();
		if(recv_char<0){
			break;
		}
	}
	recieving_data_going=-1;
	pthread_exit(0);
}

int start_socket_threads(){
	if(recieving_data_going!=1){
		int err=pthread_create(&(tid[6]),NULL,&recieving_data,NULL);
		if(err!=0){
			output("Recieving_data start: failed- error");
		}
		else{
			output("Recieving_data start: success");
			recieving_data_going=1;
		}
	}
	else{
		output("Recieving_data start: failed- already going");
	}
}

int networking(){
	char buf[100];
	output("Starting networking");
	
	sprintf(buf,"Using server: %s",SERVER);
	output(buf);
	
	if(init(NETWORKING)!=1){
		output("Init failed");
		close();
		return -1;
	}
	else{
		start_socket_threads();
		char c;
		output("tere");
		sleep(1);
		while(main_going==1 && is_socket_set==1){
			process_frame();
			usleep(1);
		}
		return close();
	}
}

void close_networking(){
	if(is_socket_set==1){
		is_socket_set=-1;
		close(sock);
	}
}

int Carlos(){
	char question;
	output("Starting Carlos");
	output("Init check:");
	int should_robot_be_running=-1;
	if(init(CARLOS)!=1){
		output("Init:	fail");
		close();
		return -1;
	}
	else{
		output("Init:	sucess");
		while(main_going==1){
			set_charge_coilgun();
			set_ping_coilgun();
			if(robot_going==1 && should_robot_be_running==1){
				should_robot_be_running=-1;
				start_find_ball();
				is_robot_set=start_robot();
			}
			if(robot_going!=1 && should_robot_be_running!=1){
				output("Do you want to start application (y/n)");
				std::cin >>question;
				if(question=='y'){
					should_robot_be_running=1;
					output("ROBOT IS READY; MAKE ADDITIONAL CHECK WITH NO POWER");
				}
				else{
					should_robot_be_running=-1;
					break;
				}
			}
			usleep(100000);
		}
		
		return close();;
	}
}
	
//-----------------------Networking----------------


//-----------------------Main----------------------



int main(int argc,char *argv[]){
	
	if(argc==2 && strcmp(argv[1],"Carlos")==0){
		return Carlos();
	}
	else if(argc==2 && strcmp(argv[1],"gui")==0){
		return gui();
	}
	else if(argc==3 && strcmp(argv[1],"networking")==0){
		memcpy(SERVER,argv[2],15);
		return networking();
	}
	else{
		output("Closing");
		output("run with sudo ./robot main or sudo ./robot vision or sudo ./robot networking xxx.xxx.xxx.xxx");
		return -1;
	}
}

//-----------------------END---------------------------
