#include <stdio.h>
#include <stdlib.h>
#include <cstring>

using namespace std;


char *buffer;
int fileLen;
int headerLen;

const int ylen = (1<<4);
const int ulen = (1<<6);
const int vlen = (1<<6);

// Must match indicies in colors.txt!!
enum Colors{
  ColorUndefined = 0,
  ColorOrange,
  ColorYellow,
  ColorBlue,
  ColorGreen,
  ColorWhite,
  ColorBlack,
  NumColors
};

void modifyBuffer() {
	//l = i*num_j*num_k + j*num_k + k;
	for (int y = 0; y < ylen; y++) {
		for (int u = 0; u < ulen; u++) {
			for (int v = 0; v < vlen; v++) {
				int i = headerLen + y*ulen*vlen + u*vlen + v;
				if (y < 6*ylen/16)
					if (buffer[i] == ColorUndefined || buffer[i] == ColorYellow) buffer[i] = ColorBlack;
			
				if (y >= 13*ylen/16)
					if (buffer[i] != ColorOrange) buffer[i] =  ColorWhite;
			}
		}
	}
}

void readFile(char * filename);
void writeFile(char * filename);

int main(int argc, char **argv) {
	if (argc != 3) {
		printf("usage: thresh_tweaker {original .tm file} {tweaked .tm file}\n");
		return 0;
	}
	readFile(argv[1]);
	modifyBuffer();
	writeFile(argv[2]);
	free(buffer);
	return 0;
}

void readFile(char *filename) {
	FILE *file;	
	file = fopen(filename, "rb");
	if (!file)
	{
		fprintf(stderr, "Unable to open file %s", filename);
		return;
	}
	
	//Get file length
	fseek(file, 0, SEEK_END);
	fileLen=ftell(file);
	fseek(file, 0, SEEK_SET);
	headerLen = fileLen - ylen*ulen*vlen;

	printf("fileLen: %d, headerLen: %d\n", fileLen, headerLen);

	//Allocate memory
	buffer = (char*) malloc(fileLen+1);
	if (!buffer)
	{
		fprintf(stderr, "Memory error!");
        fclose(file);
		return;
	}
	fread(buffer, 1, fileLen, file);
	fclose(file);
}

void writeFile(char * filename) {
	FILE *file;
	file = fopen(filename, "wb");
	if (!file) {
		fprintf(stderr, "Unable to create file %s", filename);
		free(buffer);
		return;
	}
	fwrite(buffer, 1, fileLen, file);
	fclose(file);
}
