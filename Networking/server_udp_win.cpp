
#include <windows.h>
#include <stdio.h>
#include <winsock2.h>

#define BUFLEN 1024
#define PORT 8888



//Forward declarations
    //Global
        int init_variables();
        int close();
    //Winsock
        int set_socket();
        DWORD WINAPI recv_data(void *data);
        int send_data(char *send_buffer);
    //Read char input
        int getKey();

//Variables

    //Global
        int main_going;

    //Winsock
        SOCKET sock;
        struct sockaddr_in server, si_other;
        int send_len , recv_len;
        char buf[BUFLEN];
        WSADATA wsa;

        int send_data_allowed=-1;

    //Read char input
        const short unsigned int key_a=65;
        const short unsigned int key_b=66;
        const short unsigned int key_c=67;
        const short unsigned int key_d=68;
        const short unsigned int key_e=69;
        const short unsigned int key_f=70;
        const short unsigned int key_g=71;
        const short unsigned int key_h=72;
        const short unsigned int key_i=73;
        const short unsigned int key_j=74;
        const short unsigned int key_k=75;
        const short unsigned int key_l=76;
        const short unsigned int key_m=77;
        const short unsigned int key_n=78;
        const short unsigned int key_o=79;
        const short unsigned int key_p=80;
        const short unsigned int key_q=81;
        const short unsigned int key_r=82;
        const short unsigned int key_s=83;
        const short unsigned int key_t=84;
        const short unsigned int key_u=85;
        const short unsigned int key_v=86;
        const short unsigned int key_w=87;
        const short unsigned int key_x=88;
        const short unsigned int key_y=89;
        const short unsigned int key_z=90;
        const short unsigned int key_1=49;
        const short unsigned int key_2=50;
        const short unsigned int key_3=51;
        const short unsigned int key_4=52;
        const short unsigned int key_5=53;
        const short unsigned int key_6=54;
        const short unsigned int key_7=55;
        const short unsigned int key_8=56;
        const short unsigned int key_9=57;
        const short unsigned int key_0=48;
        const short unsigned int key_LEFT=37;
        const short unsigned int key_RIGHT=39;
        const short unsigned int key_UP=38;
        const short unsigned int key_DOWN=40;
        const short unsigned int key_DEL=8;
        const short unsigned int key_SPACE=32;
        const short unsigned int key_ENTER=13;
        const short unsigned int key_LEFT_UP=36;
        const short unsigned int key_RIGHT_UP=33;
        const short unsigned int key_STOP=12;
        const short unsigned int key_LEFT_DOWN=35;
        const short unsigned int key_RIGHT_DOWN=34;


//---------------------Global--------------------------

int init_variables(){
    main_going=0;
}

int close(){
    printf("About to exit\n");
    main_going=-1;
    Sleep(2000);
    closesocket(sock);
    WSACleanup();
    Sleep(5000);
}

//----------------------Socket--------------------------
int set_socket(){
    send_len=sizeof(si_other);
    if(WSAStartup(MAKEWORD(2,2),&wsa)!=0){
        printf("WSA startup Failed");
        return -1;
    }
    else{
        if((sock=socket(AF_INET,SOCK_DGRAM,0))==INVALID_SOCKET){
            printf("Could not create socket : %d" , WSAGetLastError());
            return -1;
        }
        else{
            printf("Socket created\n");
            server.sin_family = AF_INET;
            server.sin_addr.s_addr = INADDR_ANY;
            server.sin_port = htons( PORT );
            if( bind(sock ,(struct sockaddr *)&server , sizeof(server)) == SOCKET_ERROR){
                printf("Bind failed with error code : %d" , WSAGetLastError());
                return -1;
            }
            else{
                printf("socket set\n");
                return 0;
            }
        }
    }
}

DWORD WINAPI recv_data(void *data){
    char read_buffer[BUFLEN];
    char *frame_buffer;
    int frame_revs;
    int frame_counted=0;
    int frame_set=-1;
    char frame_end[100];
    while(main_going==0){
        if ((recv_len = recvfrom(sock, read_buffer, BUFLEN, 0, (struct sockaddr *) &si_other, &send_len)) == SOCKET_ERROR){
            printf("recvfrom() failed with error code : %d" , WSAGetLastError());
            close();
        }
        else{
            if(send_data_allowed!=1){
                send_data_allowed=1;
            }
           if(recv_len>3 && strncmp(read_buffer,"frs-",4)==0){
                printf("siin\n");
                frame_revs=atoi(strtok(read_buffer,"frs-"));
                frame_buffer=(char*)malloc(frame_revs*1023+1);
                frame_set=1;
                sprintf(frame_end,"fre-%i",frame_revs);
            }
            else if(strcmp(read_buffer,frame_end)==0){
                frame_buffer[frame_counted*1023]='\0';
                //printf("strlen buf %i\n",strlen(frame_buffer));
                frame_set=0;
                free(frame_buffer);
            }
            else if(frame_set==1){
                if(frame_counted>=frame_revs){
                    frame_set=0;
                    frame_counted=0;
                }
                else{
                    printf("frame\n");
                    fflush(stdout);
                    memcpy(frame_buffer+1023*frame_counted,read_buffer,recv_len);
                    frame_counted++;
                }
            }
            else{
                printf("Recvied: %s\n",read_buffer);
            }
        }

        fflush(stdout);
        memset(read_buffer,'\0',BUFLEN);
    }
    printf("recieving data over\n");
}

int start_recv_data(){
    HANDLE thread=CreateThread(NULL,0,recv_data,NULL,0,NULL);

}

int send_data(char *send_buffer){
    if(send_data_allowed==1){
        if (sendto(sock, send_buffer, strlen(send_buffer), 0, (struct sockaddr*) &si_other, send_len) == SOCKET_ERROR){
            printf("sendto() failed with error code : %d" , WSAGetLastError());
            close();
        }
    }
}

//---------------------Read char input---------------------

char convert_key_to_char(int i){
    switch(i){
        case key_a:
            return 'a';
            break;
        case key_b:
            return 'b';
            break;
        case key_c:
            return 'c';
            break;
        case key_d:
            return 'd';
            break;
        case key_e:
            return 'e';
            break;
        case key_f:
            return 'f';
            break;
        case key_g:
            return 'g';
            break;
        case key_h:
            return 'h';
            break;
        case key_i:
            return 'i';
            break;
        case key_j:
            return 'j';
            break;
        case key_k:
            return 'k';
            break;
        case key_l:
            return 'l';
            break;
        case key_m:
            return 'm';
            break;
        case key_n:
            return 'n';
            break;
        case key_o:
            return 'o';
            break;
        case key_p:
            return 'p';
            break;
        case key_q:
            return 'q';
            break;
        case key_r:
            return 'r';
            break;
        case key_s:
            return 's';
            break;
        case key_t:
            return 't';
            break;
        case key_u:
            return 'u';
            break;
        case key_v:
            return 'v';
            break;
        case key_w:
            return 'w';
            break;
        case key_x:
            return 'x';
            break;
        case key_y:
            return 'y';
            break;
        case key_z:
            return 'z';
            break;
        case key_1:
            return '1';
            break;
        case key_2:
            return '2';
            break;
        case key_3:
            return '3';
            break;
        case key_4:
            return '4';
            break;
        case key_5:
            return '5';
            break;
        case key_6:
            return '6';
            break;
        case key_7:
            return '7';
            break;
        case key_8:
            return '8';
            break;
        case key_9:
            return '9';
            break;
        case key_0:
            return '0';
            break;
        case key_SPACE:
            return ' ';
            break;
        default:
            return '\n';
    }
}


int getKey(char *buffer){
    int i;
    char c;
    int buffer_index=0;
    while(main_going==0){
        Sleep(10);
        for(i=8;i<=256;i++){
            if(GetAsyncKeyState(i) & 0x7FFF){

                if(i!=key_ENTER && i!=key_LEFT && i!=key_RIGHT && i!=key_UP && i!=key_DOWN && i!=key_DEL && i!=key_STOP && i!=key_RIGHT_UP && i!=key_RIGHT_DOWN && i!=key_LEFT_DOWN && i!=key_LEFT_UP){
                    c=convert_key_to_char(i);
                    if(c!='\n'){
                        printf("%c",c);
                        buffer[buffer_index++]=c;
                    }
                }
                else{
                    switch(i){
                        case key_ENTER:
                            printf("\n");
                            buffer[buffer_index]='\0';
                            return buffer_index;
                            break;
                        case key_LEFT:
                            send_data("tl");
                            break;
                         case key_UP:
                            send_data("fw");
                            break;
                        case key_RIGHT:
                            send_data("tr");
                            break;
                        case key_DOWN:
                            send_data("bw");
                            break;
                         case key_DEL:
                            break;
                        case key_RIGHT_DOWN:
                            send_data("rd");
                            break;
                        case key_RIGHT_UP:
                            send_data("ru");
                            break;
                        case key_LEFT_DOWN:
                            send_data("ld");
                            break;
                        case key_LEFT_UP:
                            send_data("lu");
                            break;
                        case key_STOP:
                            send_data("st");
                            break;
                         default:
                            break;
                    }
                }
            }
        }
    }
}

//------------------Read char input-----------------------------------
int main()
{
    if(set_socket()!=0){
        close();
    }
    else{
        int char_read;
        char buffer[1024];
        main_going=0;
        start_recv_data();
        printf("Socket set\n");
        while(main_going==0){
            char_read=getKey(buffer);
            send_data(buffer);
            if(strcmp(buffer,"quit")==0){
                close();
            }
            Sleep(100);
        }
    }
}
