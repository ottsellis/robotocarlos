ROBOTO CARLOS

06.11.2013

Panen igaks juhuks siia väiske käivitamisjuhendi.
	Valikud on nüüd hoopis järgnevad:
		1)	sudo ./robot Carlos
		2) sudo ./robot GUI
		3) sudo ./robot networking xxx.xxx.xxx.xxx IP aadress, mis on selle arvuti IP address kus kohas käivitati
				./Most_recent/network_test/server_win_upd.exe
		
	Leidsin 2 kindlat viga koodis, mis seostuvad sellega, et ta peale pikka sõitu kokku jookseb, vajaks testimist
	Leidsin ühe vea miks ta ID-si esimese korraga õigesti ei loe.
	
	EI leidnud ühtegi viga seoses sellega miks mehaanik+elektroonik koodi käima ei saanud.
	
31.10.2013
Panen kirja enda plaanid/ prognoosid mida kavatsen veel Carlosega teha (loodan, et jõuan ning saan hakkama)
Esiteks arvestades seda, et ma järgmine nv lätti lähen (7-10), siis mul väga palju aega panustada ei ole. Teiseks kõik plaanid
mida veel teha, oleksid piisavalt suured ning riskantsed, st asjad võivad valesti minna. Sellessuhtes paarI muudatusega 
on hetkel olemas kood, mida teoreetiliselt saab kasutada Robotexil ning millega ma arvan, et me häbisse ei jää.
Seda arvestades asjad mida ma tahaksin veel teha ning asjad mida teised peaksid tegema, et kõik toimiks:
* Super värvi confid, st vaatasin just meie TTÜ pilte ning neid me kasutada ei saa (vale suurus). Seega ei tea mis saab, 
kuidagi tuleks välja mõelda miks Kurja conf ei töötanud. Ott äkki sulle midagi. (äkki saab veel millalgi TTÜ-s käia vms)
* Joone tuvastuse algortim ülekäia ning see sobilikuks tuunida. Lõpp kiirused võiksid jääda kuskile 100 kanti.
* Igaksjuhuk mingi failsafe väravaotsimisele, kui ta ühe tiiruga väravat ei leia, et ta lööks siis suvalt vms
* Mingisugune algne värava meelde jätmise värgendus; st ainult niipalju, et Carlos hakkaks õigele poole pöörama, prob võidab
sellega 80 % juhtudel 0.5 sekki. Ehk mitte kõige primaarsem.
* Pallini jõudmise check, st asi millest Laane rääkis ning eile lõpuks aru sain; st suurtel kiirustel tekkivad vead on 
liiga suured ning ta sõidab pallist mööda. Arvatavasti saan selle nädala jooksul tehtud, plaan on hakata kasutama ka y
väärtusi palli leidmisel ning selle abil õige nurga väljaarvutamine.
* Kood korra kriitilise pilguga üle käia, välja visata mittevajalikud/ aeglased ideed. Testida reaalselt fps-i ning muuta
suvalised usleep-id muudetavateks. St et teoreetiliselt oleks võimalik paari muudatusega koodis muuta, kas robot mängib
oma maksimumi lähedal (e võib juhtuda et sõidab välja/ lööb mööda/ jookseb kokku / lendab õhku jne) vs robot lööb aeglaselt
11 palli sisse.

Kõike seda on plaanis teha enne kui ma lätti lähen, st viimasel nädalal Carlose koodi pm mitte muuta, vaid TESTIDA TESTIDA
TESTIDA, ehk tegelikult selle nädala jooksul tahaksin Valmis saada.

16.10.2013
Kes iganes Labori läheb, võiks mõlemad akud täis laadida (läpakas/ robot).
Karlile- tegime põhja all veits (palju) muudatusi, vahetasime ära plaadid 2/3. Ära neid tagasi vaheta, muu tee paremaks kui oskad.
Praegu jäi CG plaat lahti.

13.10.2013
Akut tuleb laadida. -- Esmaspäeval saan kella 12 ajal laadida.
Karl pane LM koodid üles ning flashimis õpetus.
Robot võttis täna palli hambusse ning sõitis kollase värava sisse palliga, niiet peaaaegu võib öelda, et Carlos skooris!

LM kood asub http://www.upload.ee/files/3638763/wheelmod.rar.html aadressil.
	Seal on terve projekt, seega veidi lihtsam majandada.
	1)Koodi võib muuta suvalises keskkonnas muuta, aga kompileerimiseks tuleb avada AtmelStudio6-s projektifail .atsln lõpuga.
	2)Kompileerimiseks build>build solution. Selle tulemusena tekib debug kausta .hex fail.
	3)Seejärel tuleb mikrokontrolleril hoida all hwb nuppu ja selle ajal korraks vajutada reset nuppu. Kui see õnnestus,
		siis mikrokontolleril ühtegi tuld ei sütti (roheline led võib põleda).
	4)Seejärel tuleb avada Flip, valida atmega32u2 mikrokontroller, avada USB ühendus, valida hex fail ja vajutada run.
	Kui USB ühendust ei õnnestu avada, siis tõenäoliselt ei õnnestunud 3. samm.
	USB faili ei soovita tuunida, muud tuuningut võiks mulle ka näidata.

LM käsud ning tagastused:
	"go\n" -> id,0,1 v id,0,0
	"gl\n" -> id,1,1\n v id,1,0
	"gb\n" -> id,2,1 v id,2,0
	"ar1\n" -> id,3,1 -> Muutumisel: id,4,go,gb,gl\n
	"ar0\n" -> id,3,0 -> Muutumisel: Midagi
	"gs1\n" -> id,5,1 -> Perioodiliselt: id,6,SPEED\n
	"gs0\n" -> id,5,0 -> Perioodiliselt: Midagi
	"?\n"	-> id,7
	"gg\n"	-> id,8
		
	Juhul kui peaks stall tekkima: id,9,STALL_LEVEL\n
	
	Juhul kui ei saa käsust aru:   id,:,käsk

Arvutis sai asju ära kustutatud, põhikataloog asub ~/RobotoCarlos/robotocarlos/Most_recent/
	Midagigi tegev programm -> main.cc
	Kompileerimiseks -> make
	Testimiseks -> sudo ./robot


Karlile-> See asi mis sa tegid läks mega vänta. St ma ei saa serial-I readida, kui ta ei tagasta newline. 
See miks terminal emulatorid seda suudavad ei tea, aga mina ei oska (ei viitsi tegeleda). Seega newline peab kõigil 
taga olema. Ma praegu tõstsin ümber ka LM1 ning LM2 SPI? pistikud, nii sain midagigi testida. Pm kuna ma midagi teha
ei saanud, siis ma kirjutan sulle hoopis siia struktuuri mida asjad võiksid tagastada, äkki sul homme vähem mõtlemist.
Teiseks, sa võiksid panna siia README-sse üles flashimise õpetuse ning repose ka source code-d, juhul kui vaja saab 
midagi muuta. Ma tekitasin ka vajalikud kaustad.

LM käsud ning tagastused:
	"go\n" -> <id,1,1>\n v <id,1,0>\n
	"gl\n" -> <id,2,1>\n v <id,2,0>\n
	"gb\n" -> <id,3,1>\n v <id,3,0>\n
	"ar1\n" -> <id,4,1> -> Muutumisel: <id,5,go,gb,gl>\n
	"ar0\n" -> <id,4,0> -> Muutumisel: Midagi
	"gs1\n" -> <id,6,1> -> Perioodiliselt: <id,7,SPEED>\n
	"gs0\n" -> <id,6,0> -> Perioodiliselt: Midagi
	
	Juhul kui peaks stall tekkima: <id,8,STALL_LEVEL>\n
	
	
LIIKUMISMOODUL 1 LISAFUNKTSIOONID
	Kui saadad liikumismoodulile järgmised käsud, siis tagastab vastavalt:
	"gl"   ---->   1 või 2         !!! TÄHELEPANU, UUENENUD !!!
	"go"   ---->   1 või 2         !!! TÄHELEPANU, UUENENUD !!!
		(et siis peaksid saama lugeda, kui inti, siiski on seal \n lõpus)
		(   ja 2 on sellepärast, et baiti 0b00000000 ei taha ta saata   )
	
	Lisaks:                        !!! TÄHELEPANU, UUENENUD !!!
	"arn" kus n on 0 või 1 (ehk siis "ar0" või "ar1" ---auto response) 
			kui n on 1, siis tagastab LM järgneva vektori <ball,go,goal> kui ühe nende olek peaks muutuma (näiteks <0,1,1>).
			kui n on 0, siis ei tagastata automaatselt midagi.
			(n ei salvestata püsimällu, see tuleb igal käivitusel seadistada, vaikimisi olek on 0)

LIIKUMISMOODUL 2 LISAFUNKTSIOONID:
	gb		---->	<b:0> (ei ole palli) <b:1> (on pall)



Compilimis juhend:
	gcc -o foo foo.c `pkg-config --cflags --libs opencv`
	Ülevaltoodud komad on shift+üks nupp vasakule backspace-st; mitte see koma millega C-s char-i märgid.
	Kui asi sisaldab thtead-i, siis vist tuleb kompileerida järgnevalt
	
	gcc -o foo foo.c `pkg-config --cflags --libs opencv` -lpthread
	
Git-i juhend:
	Üleslaadimine:
		git add
		git commit -m "mida tegid"
		git push -u origin master
	
	Allalaadimine:
		git pull
		
			
Põhiliselt Otile, ideed mida võiks minumeelest järgida: 
	Kõik tmp(x).c failid on pm skriptimiseks ning testimiseks (kui viitsib lisab sinna ka tmp_liikumine vms)
	Kõik tmp(x)_c.c failid peaksid olema testimiseks töötavad st terviklikud failid.
	Kõik liikumisega seotud failid hakkavad kandma nime näiteks liikumine_main_(x).c
	Kõik videotöötlusega seotud failid hakkavad kandma nime näiteks video_main_(x).c
	Main faiides X näitab ära versiooni nime, tmp failides otseselt mitte midagi.
	
	Juhul kui teed muudatusi, siis palun vaheta ära ka versiooni number, st lihtsam ja kiirem on nii ülevaadet saada 
	kui hakata git-i commit historyt läbi lugema. Juhul kui tegeled mingi enda failiga, siis pole nii tähtis.
	
	Üldiselt keele struktuur võiks välja näha järgmine: 
		funktsioonid: väikse tähega
		muutujad: 	  Suure tähega, inglise keeles, mingil määral kirjeldav olukorda.
					  Loop-ides kasutame (i,j,k ...)
		array-d:	  kirjeldus_array
		Kommenteerimine: Mina isiklikult üldse ei viitsi sellega tegeleda, kuid funktsioone võiks kommenteerida
		järgmiselt:
					  Missugused sisendid saab ning idee järgi kust need tulema peaksid
					  Missuguse väljuni annab ning idee järgi kuhu see minema peaks
					  Missuguseid errorei-d võib juhtuda ning tagastus väärtused errorite kirjeldamiseks.
					  
		Üldjuhul tagastus väärtused errorite kirjeldamiseks:
			0 -> Kõik korras, kui funktsioon ei pea midagi tagastama
			1... -> Kõik korras, funktsioon pidi tagastama
			-1 -> süsteemi viga, st midagi läks väga perse
			-2 -> Süsteemi viga, kuid recoverable, nt on switch käskudega, kuid sisend ei map-i ühtegi.
			-3 .. -> Kirjelda funktsiooni päises.
			
		Kui tegemist on funktsiooniga mis tagastab mitte int tüüpi, siis lähtu loogiliselt, et funktsioon-i 
		tõttu midagi perse minna ei saaks. St et kui juhtub probleem, siis teda väljakutsuv funktsioon vms saaks 
		probleemist teada.
		
		
